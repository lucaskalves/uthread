#include "../include/uthread.h"
#include  <stdio.h>
#include  <stdlib.h>

umutex_t mutex;

void *func0(void *arg) {
  uthread_lock(&mutex);
  printf("func0 got in CS\n");
  uthread_yield();
  uthread_unlock(&mutex);
  printf("func0 got out of CS\n");
  return NULL;
}

void *func1(void *arg) {
  printf("func1 requested lock\n");
  uthread_lock(&mutex);
  printf("func1 got in CS\n");
  uthread_yield();
  uthread_unlock(&mutex);
  printf("func1 got out of CS\n");
  return NULL;
}

int main() {
  int id0, id1,init;

  init = libuthread_init();
  uthread_mutex_init(&mutex);

  printf("LIBUTHREADINIT status: %d\n",init);

  id0 = uthread_create(func0, NULL);
  printf("TID_1: %d\n",id0);
  id1 = uthread_create(func1, NULL);
  printf("TID_2: %d\n",id1);

  uthread_join(id0);
  uthread_join(id1);

  return 0;
}
#include "../include/uthread.h"
#include <stdlib.h>
#include <stdio.h>

void *func0(void *arg) {
  printf("executou func0\n");
  uthread_yield();
  printf("vai terminar func0\n");
  return NULL;
}

void *func1(void *arg) {
  printf("executou func1\n");
  uthread_yield();
  printf("vai terminar func1\n");
  return NULL;
}

int main(int argc, char *argv[]) {
  int id0, id1,init;

  init = libuthread_init();
  printf("LIBUTHREADINIT: %d\n",init);
  id0 = uthread_create(func0, NULL);
  printf("TID_1: %d\n",id0);
  id1 = uthread_create(func1, NULL);
  printf("TID_2: %d\n",id1);


  uthread_join(id0);
  uthread_join(id1);

  printf("vai terminar main\n");

  return 0;
}

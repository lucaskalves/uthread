#include "../include/uthread.h"
#include <stdlib.h>
#include <stdio.h>

int buffer;  // Single buffer of size 1

void* producer(void* n)
{
	int i = 0;
	int max = *(int*)n;
	printf("started producer\n");

	while (i<max)
	{
		printf("producer made %d\n", i);
  	buffer = i;
  	i = i + 1;
		uthread_yield();
	}
	
	return NULL;
}

void* consumer(void* n)
{
	int i;
	int max = *(int*)n;

	printf("started consumer\n");

	for (i=0;i<max;i++)
	{
   		printf("consumer got %d  \n",buffer);
			uthread_yield();
	}

	return NULL;
}
 
int main(int argc, char* argv[])  
{
	int n = 10;

	int consumer_tid, producer_tid;

	libuthread_init();

	producer_tid = uthread_create(producer, &n);
	consumer_tid = uthread_create(consumer, &n);

	uthread_join(producer_tid);
	printf("joined producer\n");
	uthread_join(consumer_tid);
	printf("joined consumer\n");

	printf("finished main\n");

	return 0;
}
 

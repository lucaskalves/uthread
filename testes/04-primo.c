
/* 
Programa primo.c 

Exemplo de uso de programação concorrente. 
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h> 
#include "../include/uthread.h"

#define MAX 128

int counter = 0;
umutex_t *mutex;

int isPrime(int num) {
   int i;

   if(num == 0)
     return 1;
   for(i = 2; i <= (int) sqrt((double)num); i++)
       if(num % i == 0) 
         return 0;
   return 1;
}

void *primos(void *id) {
  int i;
  long int limit = (long int) pow(10,5);

  while (counter < limit) {
    counter++;
    if (isPrime(counter))
       printf("%8d (id=%1d)", counter, (int)id);
    for (i=0; i < pow(10,5); i++); /*apenas para consumir tempo CPU*/
  }
  uthread_yield();
}

int main(int argc, char* argv[]) {
    int *id[MAX];
    int i;

     //inicializa a uthread
    int uthread_init_status = libuthread_init();
    if(uthread_init_status < 0) {
       printf("libuthread initialization error... no: %d\n",uthread_init_status);
      exit(0);
    }

    for(i=0; i < MAX; i++)
      id[i] = uthread_create((void *)(primos), (void *) i);

    for(i=0; i < MAX; i++)
      uthread_join(id[i]);
   
    exit(0);
}

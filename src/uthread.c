#include "uthread.h"
#include "scheduler.h"
#include "dispatcher.h"
#include <stdbool.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <stdio.h>

// context to be called when a thread is finished
ucontext_t* end_of_thread_context;

// size of the stack used in ucontext
static const size_t STACKSIZE = 10485760 ;

// Creates a context based on the starting routine and the next thread to be run
static ucontext_t* create_context(void * (*start_routine)(void*), void *arg, ucontext_t *next_context) {
  ucontext_t* new_context = (ucontext_t*) malloc(sizeof(ucontext_t));

  if(new_context != NULL) {
    getcontext(new_context);
    new_context->uc_stack.ss_sp = mmap(NULL,STACKSIZE,PROT_WRITE|PROT_READ,MAP_PRIVATE|MAP_GROWSDOWN|MAP_ANONYMOUS,-1,0);
    new_context->uc_stack.ss_size = STACKSIZE;
    new_context->uc_link = next_context;

    makecontext(new_context, (void (*)(void))start_routine, 1, arg);
  }

  return new_context;
}

// Creates a context based on the starting routine without args and the next thread to be run
static ucontext_t* create_context_without_args(void (*start_routine) (void), ucontext_t* next_context) {
  ucontext_t* new_context = (ucontext_t*) malloc(sizeof(ucontext_t));

  if(new_context != NULL) {
    getcontext(new_context);
    new_context->uc_stack.ss_sp = mmap(NULL,STACKSIZE,PROT_WRITE|PROT_READ,MAP_PRIVATE|MAP_GROWSDOWN|MAP_ANONYMOUS,-1,0);
    new_context->uc_stack.ss_size = STACKSIZE;
    new_context->uc_link = next_context;

    makecontext(new_context, start_routine, 0);
  }

  return new_context;
}

// Get the first thread in the ready list and run it
static int dispatch_next(){
  tcb_t* scheduled_thread = schedule();

  if(scheduled_thread != NULL) {

    // dispatch the thread
    dispatch(scheduled_thread);

    // user thread doesn't ended correctly!
    return -1;
  }

  return 0;
}

// function to be called when a thread ends. Kill the running thread and run
// the next thread in the queue
static void end_of_thread_handler() {
  tcb_t* thread = get_running_thread();
  murder(thread);
  dispatch_next();
}






/*
Initializes the uthread library

 Return:
   0 - no errors
   1 - error with the allocation of the main context
   2 - errors with the scheduler initialization
   3 - error with the creation of the end of thread handler context
   4 - errors with getcontext of the main thread
   5 - could not create the thread for the main context
   6 - reached invalid exit point
*/
int libuthread_init() {
  int dispatched_main = false;

  // allocates the main context
  ucontext_t* main_context = (ucontext_t*)malloc(sizeof(ucontext_t));
  if(main_context == NULL) {
    return 1;
  }
  
  // initializes the scheduler
  if(scheduler_init() < 0){
    return 2;
  }

  // creates the thread end of thread handler function
  end_of_thread_context = create_context_without_args(end_of_thread_handler, NULL);
  if(end_of_thread_context == NULL) {
    return 3;
  }

  // get the context before the main thread dispatch
  int result_main_context = getcontext(main_context);

  // dispatch the main thread only once
  if(!dispatched_main){
    dispatched_main = true;

    // get the context of the main function
    if(result_main_context < 0) {
      return 4;
    }

    // creates the thread for the main context
    if(create_thread(main_context) < 0) {
      return 5;
    }

    // dispatch the main thread
    if(dispatch_next() < 0) {
      return 6;
    }
  }

  return 0;
}


/*
Create a new thread

 Return:
   positive values - the id of the created thread
   negative values - some error occurred
     -1 - error with the creation of the context
     -2 - error with the creation of the thread
*/
int uthread_create(void * (*start_routine)(void*), void * arg) {
  ucontext_t* thread_context;
  int tid;

  thread_context = create_context(start_routine, arg, end_of_thread_context);

  if(thread_context == NULL) {
    return -1;
  }

  tid = create_thread(thread_context);

  if(tid < 0) {
    return -2;
  }

  return tid;
}


/*
Stop the running thread adding it in the ready queue. And also run the next thread in the queue.

 Return:
 0 - no errors
 -1 - something went wrong
*/
int uthread_yield() {

  int yielded = 0;

  tcb_t* thread = get_running_thread();

  save_context(thread);

  if (!yielded){
    yielded = 1;

    add_ready(thread);

    if (dispatch_next() == 0)
      return 0;
    else
      return -1;
  } else {
    return 0;
  }

}


/*
Blocks the running thread until the specified thread ends.

Entries:
 int thr - the tid of the thread that the current thread will wait for
Return:
 0 - no errors
 -1 - error
*/
int uthread_join(int thr){
  tcb_t* this_thread = get_running_thread();
  tcb_t* wait_for_thread = get_thread(thr);

  // thread that is blocking this thread was already finished or not created yet
  if (wait_for_thread == NULL){
    return -1;
  }

  if (add_waiting_thread(wait_for_thread, this_thread) == false) {
    return -1;
  }

  block_running_thread();
  save_context(this_thread);

  if(this_thread->status == BLOCKED) {
    return dispatch_next();
  }

  return 0;
}


/*
Try to get into a critical session

Entries:
 umutex_t lock - a mutex variable yet initialized. The mutex variable can
 be initalized with something like:

  umutex_t *mutex = (umutex_t *)malloc(sizeof(umutex_t));
  if ( mutex != NULL) {
    mutex->cs_free = true;
    mutex->waiting = tcb_queue_create();
  }

Return:
 0 - no errors
 -1 - error
*/
int uthread_lock(umutex_t* lock) {
  // checks if the critical section is free
  if(lock->cs_free) {
    lock->cs_free = false;
  } else {
    // add the current thread to the waiting list of the mutex variable
    tcb_t* this_thread = get_running_thread();
    if(!tcb_queue_enqueue(this_thread, lock->waiting)) {
      return -1;
    }

    if(block_thread(this_thread) < 0) {
      return -1;
    }

    if(save_context(this_thread) < 0) {
      return -1;
    }

    // only dispatch next thread if this thread was blocked.
    if(this_thread->status == BLOCKED) {
      dispatch_next();
    }
  }

  return 0;
}

/*
Try to get out of a critical session

Entries:
 umutex_t lock - a mutex variable yet initialized. The mutex variable can
 be initalized with something like:

  umutex_t *mutex = (umutex_t *)malloc(sizeof(umutex_t));
  if ( mutex != NULL) {
    mutex->cs_free = true;
    mutex->waiting = tcb_queue_create();
  }

  Alternatively, the mutex can be initialized with the uthread_mutex_init function.

Return:
 0 - no errors
 -1 - error
*/
int uthread_unlock (umutex_t* lock) {
  lock->cs_free = true;
  
  // put the first waiting thread in the ready queue
  tcb_t* thread = tcb_queue_dequeue(lock->waiting);
  if(thread != NULL) {
    if(add_ready(thread) < 0){
      return -1;
    }
  }

  return 0;
}


/*
Initializes the mutex variable

Entries:
  umutex_t *lock - a pointer to a mutex variable
*/
void uthread_mutex_init(umutex_t *lock) {
  lock->cs_free = true;
  lock->waiting = tcb_queue_create();
}
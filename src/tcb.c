#include "tcb.h"
#include <stdlib.h>

/* creates a new uthread tcb */
tcb_t* tcb_create(int tid, status_t status, ucontext_t* context) {
  tcb_t* thread = (tcb_t*) malloc(sizeof(tcb_t));

  if(thread != NULL) {
    thread->tid = tid;
    thread->status = status;
    thread->context = context;
    thread->waiting = tcb_list_create();
  }

  return thread;
}

// compare to threads
bool tcb_equals(tcb_t* tcba, tcb_t* tcbb){
	if (tcba->tid == tcbb->tid)
		return true;
	else
		return false;
}


// add the blocked_thread to the main_thread waiting list.
bool add_waiting_thread(tcb_t* main_thread, tcb_t* blocked_thread) {
  return tcb_list_add(main_thread->waiting, blocked_thread);
}

#include "tcb_list.h"

#include <stdlib.h>
#include <stdio.h>

tcb_list_t* tcb_list_create()
{
	tcb_list_t* list = (tcb_list_t*)malloc(sizeof(tcb_list_t));

	if(list != NULL)
	{
		list->first = NULL;
		list->last = NULL;
	}

	return list;
}

bool tcb_list_is_empty(tcb_list_t* tcb_list)
{
	if(tcb_list == NULL)
		return true;
	else if (tcb_list->first == NULL && tcb_list->last == NULL)
		return true;
	else
		return false;
}

bool tcb_list_add(tcb_list_t* tcb_list, tcb_t* tcb)
{
	tcb_list_node_t* node = (tcb_list_node_t*)malloc(sizeof(tcb_list_node_t));

	if(node == NULL)
		return false;

	else if(tcb_list_is_empty(tcb_list))
	{
		node->data = tcb;

		tcb_list->last = node;
		tcb_list->last->next = NULL;
		tcb_list->last->prev = NULL;

		tcb_list->first = node;
		tcb_list->first->next = NULL;
		tcb_list->first->prev = NULL;

		return true;
	}

	else if((tcb_list->first)->next == NULL) // one node
	{
		node->data = tcb;

		(tcb_list->first)->next = node;
		(tcb_list->last)->next = node;
		node->prev = tcb_list->last;
		node->next = NULL;

		tcb_list->last = (tcb_list->last)->next;

		return true;
	}
	else
	{
		node->data = tcb;

		(tcb_list->last)->next = node;
		node->prev = tcb_list->last;
		node->next = NULL;

		tcb_list->last = (tcb_list->last)->next;

		return true;
	}
}

tcb_t* tcb_list_remove(tcb_list_t* tcb_list, tcb_t* tcb)
{
	tcb_list_node_t* a_node = tcb_list->first;

	while(a_node != NULL)
	{
		if(tcb_equals(a_node->data, tcb))
		{
			tcb_list_node_t* to_be_removed = a_node;

			if((tcb_list->first)->next == NULL) // list with one element
			{
				tcb_list->first = NULL;
				tcb_list->last = NULL;
			}
			else if(a_node->prev == NULL) // remove the first element
			{
				tcb_list->first = (tcb_list->first)->next;
				(tcb_list->first)->prev = NULL;
			}
			else if(a_node->next == NULL) // remove last element
			{
				(a_node->prev)->next = a_node->next;
				tcb_list->last = a_node->prev;
			}
			else // default
			{
				(a_node->prev)->next = a_node->next;
				a_node->next->prev = a_node->prev;
			}

			tcb_t* removed_tcb_data = to_be_removed->data;

			free(to_be_removed);

			return removed_tcb_data;
		}		
		
		a_node = a_node->next;
	}

	return NULL;
}

void tcb_list_print(tcb_list_t* tcb_list)
{
 	printf("<[");
	if(!tcb_list_is_empty(tcb_list))
	{
		tcb_list_node_t* a_node = tcb_list->first;

		while (a_node != NULL){
			printf("%d < ", a_node->data->tid);
      		a_node = a_node->next;
		}
	}
	printf("]<");
}

tcb_t* tcb_list_get_thread(tcb_list_t* tcb_list, int tid)
{
	if(tcb_list != NULL) {
		tcb_list_node_t* a_node = tcb_list->first;

		while(a_node != NULL)
		{
			if((a_node->data)->tid == tid)
			{
				return (a_node->data);
			}

			a_node = a_node->next;
		}
	}

	return NULL;
}
#ifndef TCB_LIST_H
#define TCB_LIST_H

typedef struct tcb_list_node_st tcb_list_node_t;
typedef struct tcb_list_st tcb_list_t;

#include "tcb.h"
#include <stdbool.h>

struct tcb_list_node_st
{
	tcb_t* data;
	tcb_list_node_t* prev;
	tcb_list_node_t* next;
};

struct tcb_list_st
{
	tcb_list_node_t* first;
	tcb_list_node_t* last;
};

tcb_list_t* tcb_list_create();
bool tcb_list_is_empty(tcb_list_t* tcb_list);
bool tcb_list_add(tcb_list_t* tcb_list, tcb_t* tcb);
tcb_t* tcb_list_remove(tcb_list_t* tcb_list, tcb_t* tcb);
void tcb_list_print(tcb_list_t* tcb_list);
tcb_t* tcb_list_get_thread(tcb_list_t* tcb_list, int tid);

#endif
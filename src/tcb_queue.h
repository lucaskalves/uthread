#ifndef TCB_QUEUE_H
#define TCB_QUEUE_H

#include "tcb.h"
#include <stdbool.h>

typedef struct tcb_queue_node_st tcb_queue_node_t;
typedef struct tcb_queue_st tcb_queue_t;

struct tcb_queue_node_st {
  tcb_t* data;
  tcb_queue_node_t* next;
};

struct tcb_queue_st {
  tcb_queue_node_t* first;
  tcb_queue_node_t* last;
};

tcb_queue_t* tcb_queue_create();
bool tcb_queue_is_empty(tcb_queue_t* tcb_queue);
bool tcb_queue_enqueue(tcb_t* data, tcb_queue_t* tcb_queue);
tcb_t* tcb_queue_dequeue(tcb_queue_t* tcb_queue);
bool tcb_queue_contains(tcb_t* data, tcb_queue_t* tcb_queue);
void tcb_queue_print(tcb_queue_t* q);

#endif
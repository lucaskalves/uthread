#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "tcb.h"
#include <ucontext.h>

int scheduler_init();
int create_thread(ucontext_t* context);
int add_ready(tcb_t* thread);
tcb_t* schedule();
tcb_t* get_running_thread();
void murder(tcb_t* thread);
int block_thread(tcb_t* thread);
int block_running_thread();
tcb_t* get_thread(int tid);
void print_ready_queue();

#endif
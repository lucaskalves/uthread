#ifndef UTHREAD_H
#define UTHREAD_H

#include "tcb_queue.h"

typedef struct umutex_st umutex_t;

struct umutex_st
{
  bool cs_free;
  tcb_queue_t* waiting;
};

int libuthread_init();
int uthread_create(void * (*start_routine)(void*), void * arg);
int uthread_yield(void);
int uthread_join(int thr);
int uthread_lock(umutex_t* lock);
int uthread_unlock(umutex_t* lock);
void uthread_mutex_init(umutex_t *lock);

#endif
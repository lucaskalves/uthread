#ifndef DISPATCHER_H
#define DISPATCHER_H

#include "tcb.h"

void dispatch(tcb_t* thread);
int save_context(tcb_t* thread);

#endif
#include "dispatcher.h"
#include <ucontext.h>


// run the thread
void dispatch(tcb_t* thread) {
  setcontext(thread->context);
}

// save the context of the thread
int save_context(tcb_t* thread) {
  return getcontext(thread->context);
}
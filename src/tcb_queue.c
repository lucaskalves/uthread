#include "tcb_queue.h"
#include <stdlib.h>
#include <stdio.h>

/* creates a new tcb queue initializing its pointers */
tcb_queue_t* tcb_queue_create() {
  tcb_queue_t* q = (tcb_queue_t*)malloc(sizeof(tcb_queue_t));

  if(q != NULL) {
    q->first = NULL;
    q->last = NULL;
  }

  return q;
}


// checks if a tcb_queue is empty
bool tcb_queue_is_empty(tcb_queue_t* tcb_queue) {
  bool result = false;

  if(tcb_queue == NULL) {
    result = true;
  } else if(tcb_queue->first == NULL && tcb_queue->last == NULL) {
    result = true;
  }

  return result;
}

// enquee a thread to the end of the queue
bool tcb_queue_enqueue(tcb_t* data, tcb_queue_t* tcb_queue) {
  tcb_queue_node_t* n = (tcb_queue_node_t*) malloc(sizeof(tcb_queue_node_t));

  if(n != NULL && tcb_queue != NULL) {
    n->data = data;
    n->next = NULL;

    if(tcb_queue_is_empty(tcb_queue)) {
      tcb_queue->first = n;
      tcb_queue->last = n;
    } else {
      tcb_queue->last->next = n;
      tcb_queue->last = n;
    }

    return true;
  }

  return false;
}

// get a thread from the front of queue
tcb_t* tcb_queue_dequeue(tcb_queue_t* q) {
  tcb_t* result;
  
  if(tcb_queue_is_empty(q)) {
    return NULL;
  } else if(q->first == q->last) {
    result = q->first->data;
    q->first = NULL;
    q->last = NULL;
  } else {
    result = q->first->data;
    q->first = q->first->next;
  }

  return result;
}

// print the queue
void tcb_queue_print(tcb_queue_t* q) {
  printf("<[");
  if(!tcb_queue_is_empty(q)) {
    tcb_queue_node_t* n = q->first;

    while(n != NULL) {
      printf("%d < ", n->data->tid);
      n = n->next;
    }
  }
  printf("]<\n");
}
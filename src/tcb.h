#ifndef TCB_H
#define TCB_H

typedef struct tcb_st tcb_t;

#include <ucontext.h>
#include <stdbool.h>
#include "tcb_list.h"

typedef enum
{
  READY,
  RUNNING,
  BLOCKED
} status_t;

struct tcb_st
{
  int tid;
  status_t status;
  ucontext_t* context;
  tcb_list_t* waiting;
};

tcb_t* tcb_create(int tid, status_t status, ucontext_t* context);
bool tcb_equals(tcb_t* tcba, tcb_t* tcbb);
bool add_waiting_thread(tcb_t* main_thread, tcb_t* blocked_thread);

#endif
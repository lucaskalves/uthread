#include "cutest-1.5/CuTest.h"
#include <stdio.h>

CuSuite* TCBGetSuite();
CuSuite* TCBQueueGetSuite();
CuSuite* TCBListGetSuite();

    
void RunAllTests(void) {
  CuString *output = CuStringNew();
  CuSuite* suite = CuSuiteNew();
  
  CuSuiteAddSuite(suite, TCBGetSuite());
  CuSuiteAddSuite(suite, TCBQueueGetSuite());
  CuSuiteAddSuite(suite, TCBListGetSuite());


  CuSuiteRun(suite);
  CuSuiteSummary(suite, output);
  CuSuiteDetails(suite, output);
  printf("%s\n", output->buffer);
}

int main(void) {
  RunAllTests();
}

#include "cutest-1.5/CuTest.h"
#include "../tcb_queue.h"
#include <stdlib.h>

void test_tcb_queue_create(CuTest *tc) {
  tcb_queue_t* q = tcb_queue_create();
  
  CuAssertPtrEquals(tc, q->first, NULL);
  CuAssertPtrEquals(tc, q->last, NULL);

  free(q);
}

void test_tcb_queue_is_empty(CuTest *tc) {
  tcb_queue_t* q = tcb_queue_create();
  tcb_queue_t* null_q = NULL;

  tcb_queue_t* nonempty_q = tcb_queue_create();
  nonempty_q->first = (tcb_queue_node_t*) malloc(sizeof(tcb_queue_node_t));
  nonempty_q->last = (tcb_queue_node_t*) malloc(sizeof(tcb_queue_node_t));

  CuAssertTrue(tc, tcb_queue_is_empty(q));
  CuAssertTrue(tc, tcb_queue_is_empty(null_q));
  CuAssertTrue(tc, !tcb_queue_is_empty(nonempty_q));

  free(q);
  free(nonempty_q->first);
  free(nonempty_q->last);
  free(nonempty_q);
}

void test_tcb_queue_enqueue(CuTest *tc) {
  tcb_queue_t* q = tcb_queue_create();
  tcb_t* t = tcb_create(666, BLOCKED, NULL);
  tcb_t* t2 = tcb_create(777, READY, NULL);

  tcb_queue_enqueue(t, q);
  CuAssertTrue(tc, !tcb_queue_is_empty(q));

  tcb_queue_enqueue(t2, q);
  CuAssertTrue(tc, !tcb_queue_is_empty(q));

  free(t);
  free(t2);
  free(q->last);
  free(q->first);
  free(q);
}

void test_tcb_queue_dequeue(CuTest *tc) {
  tcb_queue_t* q = tcb_queue_create();
  tcb_t* t = tcb_create(666, BLOCKED, NULL);
  tcb_t* t2 = tcb_create(777, READY, NULL);

  tcb_queue_enqueue(t, q);
  tcb_queue_enqueue(t2, q);

  tcb_t* r1 = tcb_queue_dequeue(q);
  CuAssertTrue(tc, r1->tid == t->tid);

  tcb_t* r2 = tcb_queue_dequeue(q);
  CuAssertTrue(tc, r2->tid == t2->tid);

  CuAssertTrue(tc, tcb_queue_is_empty(q));

  free(t);
  free(t2);
  free(q->last);
  free(q->first);
  free(q);
}




/* CREATES DE CUTEST SUITE*/
CuSuite* TCBQueueGetSuite() {
  CuSuite* suite = CuSuiteNew();

  SUITE_ADD_TEST(suite, test_tcb_queue_create);
  SUITE_ADD_TEST(suite, test_tcb_queue_is_empty);
  SUITE_ADD_TEST(suite, test_tcb_queue_enqueue);
  SUITE_ADD_TEST(suite, test_tcb_queue_dequeue);
  
  return suite;
}
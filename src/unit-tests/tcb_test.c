#include "cutest-1.5/CuTest.h"
#include "../tcb.h"
#include <stdlib.h>

void test_create(CuTest *tc) {
  tcb_t* t = tcb_create(666, BLOCKED, NULL);
  
  CuAssertIntEquals(tc, t->tid, 666);
  CuAssertIntEquals(tc, t->status, BLOCKED);
  CuAssertPtrEquals(tc, t->context, NULL);
}
   
CuSuite* TCBGetSuite() {
  CuSuite* suite = CuSuiteNew();
  SUITE_ADD_TEST(suite, test_create);
  return suite;
}
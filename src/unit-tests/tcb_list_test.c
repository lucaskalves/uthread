#include "cutest-1.5/CuTest.h"
#include "../tcb_list.h"
#include <stdlib.h>

void test_tcb_list_create(CuTest *tc) {
  tcb_list_t* list = tcb_list_create();
  
  CuAssertPtrEquals(tc, list->first, NULL);
  CuAssertPtrEquals(tc, list->last, NULL);

  free(list);
}

void test_tcb_list_is_empty(CuTest *tc) {
  tcb_list_t* list = tcb_list_create();
  tcb_list_t* null_list = NULL;

  tcb_list_t* nonempty_list = tcb_list_create();
  nonempty_list->first = (tcb_list_node_t*) malloc(sizeof(tcb_list_node_t));
  nonempty_list->last = (tcb_list_node_t*) malloc(sizeof(tcb_list_node_t));

  CuAssertTrue(tc, tcb_list_is_empty(list));
  CuAssertTrue(tc, tcb_list_is_empty(null_list));
  CuAssertTrue(tc, !tcb_list_is_empty(nonempty_list));

  free(list);
  free(nonempty_list->first);
  free(nonempty_list->last);
  free(nonempty_list);
}

void test_tcb_list_add(CuTest *tc) {
  tcb_list_t* list = tcb_list_create();
  tcb_t* t = tcb_create(666, BLOCKED, NULL);
  tcb_t* t2 = tcb_create(777, READY, NULL);
  tcb_t* t3 = tcb_create(321, RUNNING, NULL);

  tcb_list_add(list, t);
  CuAssertTrue(tc, !tcb_list_is_empty(list));

  tcb_list_add(list, t2);
  CuAssertTrue(tc, !tcb_list_is_empty(list));

  tcb_list_add(list, t3);
  CuAssertTrue(tc, !tcb_list_is_empty(list));

  free(t);
  free(t2);
  free(t3);
  free(list->last);
  free(list->first);
  free(list);
}

void test_tcb_list_remove(CuTest *tc) {
  tcb_list_t* list = tcb_list_create();
  tcb_t* t = tcb_create(666, BLOCKED, NULL);
  tcb_t* t2 = tcb_create(777, READY, NULL);

  tcb_list_add(list, t);
  tcb_list_add(list, t2);

  tcb_t* r1 = tcb_list_remove(list, t2);
  CuAssertTrue(tc, r1->tid == t2->tid);

  tcb_t* r2 = tcb_list_remove(list, t);
  CuAssertTrue(tc, r2->tid == t->tid);

  CuAssertTrue(tc, tcb_list_is_empty(list));

  free(t);
  free(t2);
  free(list->last);
  free(list->first);
  free(list);
}

void test_tcb_list_get_thread(CuTest *tc){
  tcb_list_t* list = tcb_list_create();
  tcb_t* t = tcb_create(666, BLOCKED, NULL);

  tcb_list_add(list, t);

  CuAssertTrue(tc, tcb_equals(t,tcb_list_get_thread(list, t->tid)));
}

/* CREATES DE CUTEST SUITE*/
CuSuite* TCBListGetSuite() {
  CuSuite* suite = CuSuiteNew();

  SUITE_ADD_TEST(suite, test_tcb_list_create);
  SUITE_ADD_TEST(suite, test_tcb_list_is_empty);
  SUITE_ADD_TEST(suite, test_tcb_list_add);
  SUITE_ADD_TEST(suite, test_tcb_list_remove);
  SUITE_ADD_TEST(suite, test_tcb_list_get_thread);
  
  return suite;
}
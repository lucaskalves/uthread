#include "scheduler.h"
#include "tcb.h"
#include "tcb_list.h"
#include "tcb_queue.h"
#include "dispatcher.h"
#include <stdlib.h>
#include <stdio.h>

// maximum number of threads including main thread
static const int THREADS_LIMIT = 128;

// counter of the current threads (ready + running + blocked)
static int threads_counter = 0;

// counter used to genrate the tid
static int unsigned long long tid_counter = 0;

// the current running thread
tcb_t* running_thread;
// a queue of ready threads
tcb_queue_t* ready_queue;
// a list of the blocked threads
tcb_list_t* blocked_list;
// a list of all the threads
tcb_list_t* all_threads_list;


int scheduler_init() {

  // initializes the ready threads queue
  ready_queue = tcb_queue_create();
  if(ready_queue == NULL) {
    return -1;
  }

  // initializes the blocked threads list
  blocked_list = tcb_list_create();
  if(blocked_list == NULL) {
    return -2;
  }

  // initializes the all threads list
  all_threads_list = tcb_list_create();
  if(all_threads_list == NULL) {
    return -3;
  }

  threads_counter = 0;
  tid_counter = 0;

  return 0;
}


// Given a context, generate the thread tcb, add the thread to the ready
// queue and to the all threads list
int create_thread(ucontext_t* context) {
  if(threads_counter > (THREADS_LIMIT-1)) {
    return -1;
  }

  int used_tid = tid_counter;
  tcb_t* thread = tcb_create(used_tid, READY, context);

  threads_counter += 1;
  tid_counter += 1;

  if(thread == NULL) {
    return -2;
  }

  if(add_ready(thread) < 0) {
    return -3;
  }

  if(tcb_list_add(all_threads_list, thread) < 0) {
    return -4;
  }

  return used_tid;
}


// Add a thread to the ready queue and remove the thread of the blocked list
// if it is blocked
int add_ready(tcb_t* thread) {
  if(tcb_queue_enqueue(thread, ready_queue)) {
    // if the thread was blocked, unblock
    if(thread->status == BLOCKED) {
      tcb_list_remove(blocked_list, thread);
    }
    thread->status = READY;
    return 0;
  }

  return -1;
}


// Get the first thread from the ready queue and set it as the running thread
tcb_t* schedule() {
  tcb_t* scheduled = tcb_queue_dequeue(ready_queue);

  if(scheduled == NULL)
  {
    return NULL;
  }

  running_thread = scheduled;
  scheduled->status = RUNNING;

  return scheduled;
}


// Return the running thread
tcb_t* get_running_thread() {
  return running_thread;
}


// Kill the thread and add all the threads that was waiting for
// its ending to the ready queue.
void murder(tcb_t* thread) {

  // add all the waiting threads to the ready queue
  tcb_list_node_t* waiting_list_node = (thread->waiting)->first;

  while(waiting_list_node != NULL)
  {
    tcb_t* pending_thread = waiting_list_node->data;
    tcb_list_remove(thread->waiting, pending_thread);
    pending_thread->status = READY;
    add_ready(pending_thread);

    waiting_list_node = waiting_list_node->next;
  }

  threads_counter--;
  free(thread->waiting);
  free(thread);
}


// Add the thread to the blocked list
int block_thread(tcb_t* thread) {
  thread->status = BLOCKED;
  if(!tcb_list_add(blocked_list, thread)) {
    return -1;
  }

  return 0;
}


// Add the thread that is running to the blocked list
int block_running_thread() {
  return block_thread(running_thread);
}


// Return the thread tcb that has the tid passed
tcb_t* get_thread(int tid) {
  return tcb_list_get_thread(all_threads_list, tid);
}

// Print the ready queue.
void print_ready_queue(){
  tcb_queue_print(ready_queue);
}
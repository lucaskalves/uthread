# uThreads Makefile.

all: uThreads

uThreads: src/dispatcher.c src/tcb.c src/tcb_queue.c src/scheduler.c src/tcb_list.c src/uthread.c

	gcc -c src/tcb.c -o bin/tcb.o -Wall
	gcc -c src/tcb_list.c -o bin/tcb_list.o -Wall
	gcc -c src/tcb_queue.c -o bin/tcb_queue.o -Wall
	gcc -c src/dispatcher.c -o bin/dispatcher.o -Wall
	gcc -c src/scheduler.c -o bin/scheduler.o -Wall
	gcc -c src/uthread.c -o bin/uthread.o -Wall

	ar crs lib/libuthread.a bin/uthread.o bin/tcb.o bin/tcb_list.o bin/tcb_queue.o bin/dispatcher.o bin/scheduler.o

install:
	cp src/*.h include/

clean:
	rm -f bin/*.o
	rm -f lib/libuthread.a